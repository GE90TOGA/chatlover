var mod = angular.module('tvchat.services', [
	'tvchat.utils',
	'tvchat.services.userService',
  'tvchat.services.friendService',
  'tvchat.services.chatService'
]);
