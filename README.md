This is an ionic project uses firebase as realtime data service 
with facebook login allowing users to enjoy one to one chat. 


## Specification

The project is intended to follow specification from [specification](spec.md).

## Workflow
The `master` branch is protected (pushing to the branch is not possible). 
When done with a milestone, merge with a pull request.

## Project Setup

#### 1) Install ionic v1

Please follow https://ionicframework.com/docs/v1/getting-started/ to set up ionic on your computer.

#### 2) Initialise project
#### Clone project
```bash
git clone https://bitbucket.org/GE90TOGA/chatlover
```
#### Start project

```bash
ionic start YOUR_PORJ_NAME CLONED_FOLDER_PATH
```

#### 3) Install the required plugins

Under the project folder, execute:
```bash
ionic plugin add cordova-plugin-inappbrowser 
```

#### 4) Setup the required bower packages

```bash
bower install ngCordova angularfire lodash angular-moment openfb 
```

#### 5) Edit the scss/ionic.app.scss file under the scss folder so it has these

```sass
$ionicons-font-path: "../lib/ionic/fonts" !default;
$light: #fff;
$assertive: #D31996;
$positive: #19DD89;
$dark: #000;
@import url(http://fonts.googleapis.com/css?family=Oxygen);
$font-family-sans-serif: "Oxygen", "Helvetica Neue",
"Roboto", "Segoe UI", sans-serif !default;
// Include all of Ionic
@import "www/lib/ionic/scss/ionic";
@import "www/scss/app/intro";
@import "www/scss/app/search";
@import "www/scss/app/show";

```
## Run
For iOS:
```bash
ionic platform add ios
ionic build ios
ionic emulate ios
```

For iOS:
```bash
ionic platform add ios
ionic build ios
ionic emulate android
```